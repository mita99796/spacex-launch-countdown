import React from "react";
import "../design/DetailLaunch.scss";

const DetailLaunch = ({ location }) => {
  const {
    name,
    image,
    rocket,
    date,
    success,
    video,
    detail,
    images
  } = location.state;

  return (
    <div className="launch-item">
      <img
        src={image}
        alt={name}
        style={{ width: "750px", height: "750px" }}
      ></img>

      <div className="info">
        <p>{name}</p>
        <p>{rocket}</p>
        <p>{date}</p>
        <p>{success}</p>
        <p>{detail}</p>
        <a href={video}>Watch video</a>
        <div className="images">
          {images.map(item => (
            <a href={item}>
              <img
                src={item}
                alt={name}
                style={{ width: "500px", height: "500px" }}
              ></img>
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DetailLaunch;

//rafcp
