import React from "react";
import ReactDOM from "react-dom";
import Home from "./Home";
import DetailLaunch from "./pages/DetailLaunch";
import { BrowserRouter, Route } from "react-router-dom";

ReactDOM.render(
  <BrowserRouter>
    <Route exact path="/">
      <Home />
    </Route>
    <Route path="/launches/:launchID" component={DetailLaunch} />
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
