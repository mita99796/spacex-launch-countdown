import * as moment from "moment";
export function compareName(a, b) {
  if (a.mission_name < b.mission_name) {
    return -1;
  }
  if (a.mission_name > b.mission_name) {
    return 1;
  }
  return 0;
}

export function compareRocket(a, b) {
  if (a.rocket.rocket_name < b.rocket.rocket_name) {
    return -1;
  }
  if (a.rocket.rocket_name > b.rocket.rocket_name) {
    return 1;
  }
  return 0;
}

export function compareSuccess(a, b) {
  if (a.launch_success < b.launch_success) {
    return -1;
  }
  if (a.launch_success > b.launch_success) {
    return 1;
  }
  return 0;
}

export function compareDates(a, b) {
  if (moment(a.launch_date_utc) < moment(b.launch_date_utc)) {
    return -1;
  }
  if (moment(a.launch_date_utc) > moment(b.launch_date_utc)) {
    return 1;
  }
  return 0;
}
