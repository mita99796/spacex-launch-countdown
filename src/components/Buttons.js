import React from "react";
import Button from "./Button";
import "../design/Buttons.scss";

function Buttons({ data, fun, item }) {
  const returnToUp = (item, click) => {
    fun(item, click);
  };

  return (
    <div className="dropdown">
      <button className="dropbtn">{item}</button>
      <div className="dropdown-content">
        {data.map(item => (
          <Button className="button" item={item} key={item} fun={returnToUp} />
        ))}
      </div>
    </div>
  );
}

export default Buttons;
