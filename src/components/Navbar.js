import React from "react";
import { Link } from "react-router-dom";
import logo from "../assets/logo.svg";

const Navbar = () => {
  return (
    <div className="navbar">
      <div className="Logo">
        <img src={logo} alt="Logo"></img>
      </div>
      <div className="Links">
        <Link to="/">Home</Link>
        <Link to="#">Rockets</Link>
        <Link to="#">Info</Link>
        <Link to="#">About</Link>
      </div>
    </div>
  );
};

export default Navbar;
