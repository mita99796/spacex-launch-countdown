import React from "react";
import "../design/Search.scss";
function SearchPart({ hook, data, fun }) {
  return (
    <div className="search-wrapper">
      <div className="search-item">
        <input
          value={data}
          type="text"
          placeholder="Search.."
          onChange={ev => {
            hook(ev.target.value);
            fun(ev.target.value);
          }}
          onKeyPress={event => {
            if (event.key === "Enter") {
              console.log("enter");
              fun(data);
            }
          }}
        ></input>
      </div>
    </div>
  );
}

export default SearchPart;
