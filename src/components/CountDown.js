import React, { useEffect, useState } from "react";
import moment from "moment";
import "../design/CountDown.scss";

export const Countdown = props => {
  const [state, updateState] = useState({
    days: undefined,
    hours: undefined,
    minutes: undefined,
    seconds: undefined
  });

  const [rocket, setRocket] = useState({
    mission: undefined,
    rocket: undefined,
    rocket_type: undefined
  });

  // useEffect(() => {
  //   const { timeTillDate } = props;
  //   const mission = timeTillDate.rocket.mission_name;
  //   const rocket = timeTillDate.rocket.rocket.rocket_name;
  //   const rocket_type = timeTillDate.rocket.rocket.rocket_type;

  //   setRocket({ mission, rocket, rocket_type });
  // });

  useEffect(() => {
    const interval = setInterval(() => {
      const { timeTillDate, timeFormat } = props;
      const mission = timeTillDate.rocket.mission_name;
      const rocket = timeTillDate.rocket.rocket.rocket_name;
      const rocket_type = timeTillDate.rocket.rocket.rocket_type;

      const then = moment(timeTillDate.time, timeFormat);
      const now = moment();
      const countdown = moment(then - now);
      const days = countdown.format("D");
      const hours = countdown.format("HH");
      const minutes = countdown.format("mm");
      const seconds = countdown.format("ss");

      updateState({ days, hours, minutes, seconds });
      setRocket({ mission, rocket, rocket_type });
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  });

  const { days, hours, minutes, seconds } = state;

  return (
    <div className="countdown">
      <h1 className="title">Time to {<br />}next launch</h1>
      <div className="pattern"></div>
      <div className="countdown-items">
        <div className="countdown-item">
          {days}
          <span>d :</span>
        </div>
        <div className="countdown-item">
          {hours}
          <span> h :</span>
        </div>
        <div className="countdown-item">
          {minutes}
          <span> m :</span>
        </div>
        <div className="countdown-item">
          {seconds}
          <span>s</span>
        </div>
      </div>
      <div className="rocket-info">
        <p> Mission : {rocket.mission}</p>
        <p> Rocket : {rocket.rocket}</p>
      </div>
    </div>
  );
};
