import React from "react";
import "../design/LaunchPart.scss";
import { Link } from "react-router-dom";

const Launch = ({
  id,
  logo,
  name,
  rocket,
  date,
  success,
  image,
  video,
  detail,
  images
}) => {
  return (
    <Link
      to={{
        pathname: `/launches/${id}`,
        state: { name, rocket, date, success, image, video, detail, images }
      }}
    >
      <div className="launch-item">
        {logo ? <img src={logo} alt={name}></img> : null}

        <div className="info">
          <p id="mission">{name}</p>
          <div>
            <span>ROCKET :</span>
            {rocket}
          </div>
          <div>
            <span>DATE :</span>
            {date}
          </div>
          <div className="status">
            <span>STATUS :</span>
            {success === "Failure" ? (
              <p className="fail">{success}</p>
            ) : (
              <p className="success">{success}</p>
            )}
          </div>
        </div>
        <div className="middle">
          <div className="text">Detail</div>
        </div>
      </div>
    </Link>
  );
};

export default Launch;
