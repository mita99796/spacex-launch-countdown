import React, { useState } from "react";

const Button = ({ item, fun }) => {
  const [click, setClick] = useState(false);
  const separate = " ";
  return (
    <div className="boxes" style={{ color: "white", padding: "5px" }}>
      {item}
      {separate}
      <input
        key={item}
        type="checkbox"
        name="check"
        value={item}
        onClick={() => {
          fun(item, click);
          setClick(!click);
        }}
      />
    </div>
  );
};

export default Button;
//rafce
