import React, { useState } from "react";
import Launch from "./Launch";
import * as compare from "../helpers/compare";
import "../design/LaunchList.scss";
import * as moment from "moment";

function LaunchList({ data, hook }) {
  const [sort, setSort] = useState(true);

  return (
    <div className="launchlist">
      <div className="filter-options">
        <div className="dropdown">
          <button className="dropbtn">Sort by :</button>
          <div className="dropdown-content">
            <p
              className="item"
              onClick={() => {
                let pom = data.sort(compare.compareName);

                if (sort) {
                  pom.reverse();
                }

                setSort(!sort);

                hook([...pom]);
              }}
            >
              Name
            </p>

            <p
              className="item"
              onClick={() => {
                let pom = data.sort(compare.compareRocket);

                if (sort) {
                  pom.reverse();
                }

                setSort(!sort);

                hook([...pom]);
              }}
            >
              Rocket
            </p>
            <p
              className="item"
              onClick={() => {
                let pom = data.sort(compare.compareDates);

                if (sort) {
                  pom.reverse();
                }

                setSort(!sort);

                hook([...pom]);
              }}
            >
              Date
            </p>

            <p
              className="item"
              onClick={() => {
                let pom = data.sort(compare.compareSuccess);

                if (sort) {
                  pom.reverse();
                }

                setSort(!sort);

                hook([...pom]);
              }}
            >
              Success/Fail
            </p>
          </div>
        </div>
      </div>
      <div className="launches">
        {data.map(item => (
          <Launch
            date={moment(item.launch_date_local).format("LLL")}
            success={item.launch_success ? "Success" : "Failure"}
            logo={item.links.mission_patch_small}
            key={item.flight_number}
            name={item.mission_name}
            rocket={item.rocket.rocket_name}
            id={item.flight_number}
            image={item.links.mission_patch}
            video={item.links.video_link}
            detail={item.details}
            images={item.links.flickr_images}
          />
        ))}
      </div>
    </div>
  );
}

export default LaunchList;
