import React, { useEffect, useState } from "react";
import axios from "axios";
import { Countdown } from "./components/CountDown";
import Spinner from "./components/Spinner";
import SearchPart from "./components/SearchPart";
import "./design/App.scss";
import LaunchList from "./components/LaunchList";
import * as moment from "moment";
import Buttons from "./components/Buttons";
import Navbar from "./components/Navbar";
import { FaArrowDown } from "react-icons/fa";
// import NextRocketLaunch from "./components/NextRocketLaunch";

function Home() {
  const [launches, addLaunch] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [years, addYear] = useState([]);
  const [rockets, addRockets] = useState([]);
  const [term, updateTerm] = useState("");
  const [loading, setLoading] = useState(true);

  // * Functions

  const calulateTime = () => {
    let countdowntime = new moment(
      launches
        .map(item => item.launch_date_utc)
        .find(item => moment() < new moment(item))
    );
    let rocket = launches.find(
      item => moment() < new moment(item.launch_date_utc)
    );

    // return rocket;

    return {
      time: countdowntime.subtract(1, "days").subtract(1, "hours"),
      rocket
    };
  };

  // const nextRocket = () => {
  //   let rocket = launches.find(
  //     item => moment() < new moment(item.launch_date_utc)
  //   );
  //   console.log(rocket);
  //   return rocket;
  // };

  const filterLaunches = text => {
    if (filtered.length > 0) {
      let pom = filtered.filter(
        item =>
          item.mission_name.toUpperCase().includes(text.toUpperCase()) ||
          item.rocket.rocket_name.toUpperCase().includes(text.toUpperCase())
      );

      if (text === "") {
        setFiltered([]);
      } else {
        setFiltered([...pom]);
      }
    } else {
      let pom = launches.filter(
        item =>
          item.mission_name.toUpperCase().includes(text.toUpperCase()) ||
          item.rocket.rocket_name.toUpperCase().includes(text.toUpperCase())
      );

      if (text === "") {
        setFiltered([]);
      } else {
        setFiltered([...pom]);
      }
    }
  };

  const fetch = async () => {
    const result = await axios.get("https://api.spacexdata.com/v3/launches");

    addLaunch(result.data);
    const year = [...new Set(result.data.map(item => item.launch_year))];
    addYear([...year]);

    const rocket = [
      ...new Set(result.data.map(item => item.rocket.rocket_name))
    ];
    addRockets([...rocket]);
    setLoading(false);
  };

  const filterYear = (year, clicked) => {
    if (filtered.length > 0 && clicked) {
      //TODO izbaci iz niza tu godinu
      let pom = filtered.filter(item => item.launch_year !== year);
      debugger;
      setFiltered([...pom]);
    } else if (filtered.length > 0 && !clicked) {
      //TODO ubaci u niz filtriranu godinu
      let pom = filtered.filter(item => item.launch_year === year);

      if (pom.length === 0) {
        pom = launches.filter(item => item.launch_year === year);
        setFiltered([...filtered, ...pom]);
      } else {
        setFiltered([...pom]);
      }
    } else {
      //TODO prvu godinu u niz filtriranih
      let pom = launches.filter(item => item.launch_year === year);
      debugger;
      setFiltered([...pom]);
    }
  };

  const filterRocket = (rocket, clicked) => {
    if (filtered.length > 0 && clicked) {
      //TODO izbaci iz niza tu raketu
      let pom = filtered.filter(item => item.rocket.rocket_name !== rocket);
      debugger;
      setFiltered([...pom]);
    } else if (filtered.length > 0 && !clicked) {
      //TODO ubaci u niz filtriranu raketu
      let pom = filtered.filter(item => item.rocket.rocket_name === rocket);
      debugger;
      if (pom.length === 0) {
        pom = launches.filter(item => item.rocket.rocket_name === rocket);
        setFiltered([...filtered, ...pom]);
      } else {
        setFiltered([...pom]);
      }
    } else {
      //TODO prvu raketu u niz filtriranih
      let pom = launches.filter(item => item.rocket.rocket_name === rocket);
      setFiltered([...pom]);
      debugger;
    }
  };

  useEffect(() => {
    fetch();
  }, []);

  return (
    <div className="App">
      <Navbar />
      <div className="vl">
        <p id="scroll-down">Scroll down</p>
        <FaArrowDown className="arrow" />
      </div>
      <div className="section-countdown">
        <Countdown
          timeTillDate={calulateTime()}
          timeFormat="MM DD YYYY, h:mm a"
        />
      </div>
      <div className="grid-container">
        <div className="left-menu">
          <SearchPart fun={filterLaunches} hook={updateTerm} data={term} />
          <Buttons data={years} fun={filterYear} item="Years" />
          <Buttons data={rockets} fun={filterRocket} item="Rockets" />

          <div className="dropdown">
            <button className="dropbtn">Rocket-status</button>
            <div className="dropdown-content">
              <div className="success-fail">
                Success :
                <input
                  name="check"
                  type="checkbox"
                  onClick={ev => {
                    if (filtered.length > 0) {
                      let pom = filtered.filter(
                        item => item.launch_success === true
                      );
                      setFiltered([...pom]);
                    } else {
                      let pom = launches.filter(
                        item => item.launch_success === true
                      );
                      setFiltered([...pom]);
                    }
                  }}
                />
                Fail :
                <input
                  name="check"
                  type="checkbox"
                  onClick={ev => {
                    if (filtered.length > 0) {
                      let pom = filtered.filter(
                        item => item.launch_success === false
                      );
                      setFiltered([...pom]);
                    } else {
                      let pom = launches.filter(
                        item => item.launch_success === false
                      );
                      setFiltered([...pom]);
                    }
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        {loading ? (
          <Spinner />
        ) : (
          <div>
            {filtered.length === 0 && term.length === 0 ? (
              <LaunchList data={launches} hook={addLaunch} />
            ) : filtered.length > 0 ? (
              <LaunchList data={filtered} hook={setFiltered} />
            ) : (
              <p>NISTA</p>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default Home;
